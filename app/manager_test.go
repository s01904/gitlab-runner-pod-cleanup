// +build !integration

package app

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/config"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/kubeclient"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/logging"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sFakeClient "k8s.io/client-go/rest/fake"
)

const (
	customTTL     = "custom/ttl"
	ttl10s        = "10s"
	ttl30s        = "30s"
	namespace     = "ns-test"
	podCleanupTTL = "pod-cleanup/ttl"
)

var (
	errClientFunc = fmt.Errorf("unexpected request")

	thirtySecAgo = getMetaTime(-30)
	tenSecAgo    = getMetaTime(-10)

	contextTimeout = 30 * time.Second
)

func TestNewPodsManager(t *testing.T) {
	logger := logging.New()
	err := logger.SetFormat(logging.FormatJSON)
	assert.NoError(t, err)

	fake := kubeclient.GetTestKubernetesClient(podsAPIVersion, nil)

	tests := map[string]struct {
		opts               newPodsManagerOpts
		expectedPodManager *PodsManager
		verifyResult       func(*testing.T, error)
	}{
		"generate successfully a PodManager": {
			opts: newPodsManagerOpts{
				logger: logger,
				cfg: config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Interval:             "1s",
					CacheCleanupInterval: "30m",
					CacheExpiration:      "1.5h",
					Limit:                10,
					Kubernetes: config.KubernetesConfig{
						RequestLimit:   10,
						RequestTimeout: "60s",
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
					},
				},
				namespace:  namespace,
				kubeClient: fake,
			},
			verifyResult: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"fail to generate PodManager because of invalid cache cleanup interval": {
			opts: newPodsManagerOpts{
				logger: logger,
				cfg: config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Interval:             "1s",
					CacheCleanupInterval: "faulty",
					CacheExpiration:      "1.5h",
					Limit:                10,
					Kubernetes: config.KubernetesConfig{
						RequestLimit:   10,
						RequestTimeout: "60s",
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
					},
				},
				namespace:  namespace,
				kubeClient: fake,
			},
			verifyResult: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "parsing CacheCleanupInterval")
			},
		},
		"fail to generate PodManager because of invalid cache expiration": {
			opts: newPodsManagerOpts{
				logger: logger,
				cfg: config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Interval:             "1s",
					CacheCleanupInterval: "30m",
					CacheExpiration:      "faulty",
					Limit:                10,
					Kubernetes: config.KubernetesConfig{
						RequestLimit:   10,
						RequestTimeout: "60s",
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
					},
				},
				namespace:  namespace,
				kubeClient: fake,
			},
			verifyResult: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "parsing CacheExpiration")
			},
		},
		"fail to generate PodManager because of invalid request timeout": {
			opts: newPodsManagerOpts{
				logger: logger,
				cfg: config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Interval:             "1s",
					CacheCleanupInterval: "30m",
					CacheExpiration:      "30m",
					Limit:                10,
					Kubernetes: config.KubernetesConfig{
						RequestLimit:   10,
						RequestTimeout: "faulty",
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
					},
				},
				namespace:  namespace,
				kubeClient: fake,
			},
			verifyResult: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "parsing Kubernetes RequestTimeout")
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer
			logger.SetOutput(&buf)

			_, err := NewPodsManager(tc.opts)
			tc.verifyResult(t, err)
		})
	}
}

func TestRetrieveAllPods(t *testing.T) {
	count := 5
	limit := 10
	version, codec := kubeclient.TestVersionAndCodec()

	getConfig := func() config.Config {
		return config.Config{
			LogLevel:             "debug",
			LogFormat:            logging.FormatText,
			Interval:             "1s",
			CacheCleanupInterval: "30m",
			CacheExpiration:      "1.5h",
			Limit:                limit,
			Kubernetes: config.KubernetesConfig{
				RequestLimit:   limit,
				RequestTimeout: "60s",
				Namespaces:     []string{namespace},
				Annotation:     podCleanupTTL,
			},
		}
	}

	podList := generatePodList(count, namespace, nil, metav1.Time{})
	assert.NotNil(t, podList)

	logger := logging.New()
	err := logger.SetFormat(logging.FormatJSON)
	assert.NoError(t, err)

	tests := map[string]struct {
		config         func() config.Config
		clientFunc     func(*http.Request) (*http.Response, error)
		expectedResult *corev1.PodList
		expectedErr    error
		verifyResult   func(*testing.T, *corev1.PodList, *corev1.PodList)
	}{
		"retrieve empty list of pods": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := &corev1.PodList{
						TypeMeta: metav1.TypeMeta{
							Kind:       "",
							APIVersion: "",
						},
						Items: nil,
					}
					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, pods),
					}, nil
				default:
					return nil, errClientFunc
				}
			},
			expectedResult: &corev1.PodList{
				TypeMeta: metav1.TypeMeta{
					Kind:       "",
					APIVersion: "",
				},
				Items: nil,
			},
			verifyResult: func(t *testing.T, expected, actual *corev1.PodList) {
				assert.Equal(t, expected.Kind, actual.Kind)
				assert.Equal(t, expected.APIVersion, actual.APIVersion)
				assert.ElementsMatch(t, expected.Items, actual.Items)
			},
		},
		"retrieve not empty list of pods": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, podList),
					}, nil
				default:
					// Ensures no GET is performed when deleting by name
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			expectedResult: podList,
			verifyResult: func(t *testing.T, expected, actual *corev1.PodList) {
				assert.Equal(t, expected.Kind, actual.Kind)
				assert.Equal(t, expected.APIVersion, actual.APIVersion)
				assert.ElementsMatch(t, expected.Items, actual.Items)
				assert.Len(t, actual.Items, count)
			},
		},
		"fail to retrieve list of pods": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				return nil, errClientFunc
			},
			expectedErr: fmt.Errorf(
				"manager encountered an error: Get http://localhost/api/%s/namespaces/%s/pods?limit=%d: %s, retrieving pod list",
				podsAPIVersion,
				namespace,
				limit,
				errClientFunc,
			),
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer
			logger.SetOutput(&buf)

			fake := kubeclient.GetTestKubernetesClient(podsAPIVersion, k8sFakeClient.CreateHTTPClient(tc.clientFunc))

			podMgrOpts := newPodsManagerOpts{
				logger:     logger,
				cfg:        tc.config(),
				namespace:  namespace,
				kubeClient: fake,
			}
			pm, err := NewPodsManager(podMgrOpts)
			require.NoError(t, err)

			err = pm.logger.SetLevel(podMgrOpts.cfg.LogLevel)
			require.NoError(t, err)

			ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
			defer cancel()

			pods, err := pm.retrieveAllPods(ctx, "")

			if tc.verifyResult != nil {
				tc.verifyResult(t, tc.expectedResult, pods)
			}

			if tc.expectedErr != nil {
				assert.Error(t, err)
				assert.EqualError(t, err, tc.expectedErr.Error())
				assert.Nil(t, pods)
			}
		})
	}
}

func TestExecuteDeletion(t *testing.T) {
	version, codec := kubeclient.TestVersionAndCodec()

	logger := logging.New()
	err := logger.SetFormat(logging.FormatJSON)
	assert.NoError(t, err)

	getConfig := func() config.Config {
		return config.Config{
			LogLevel:             "debug",
			LogFormat:            logging.FormatText,
			Interval:             "1s",
			CacheCleanupInterval: "30m",
			CacheExpiration:      "1.5h",
			Limit:                10,
			MaxErrAllowed:        5,
			Kubernetes: config.KubernetesConfig{
				RequestLimit:   10,
				RequestTimeout: ttl10s,
				Namespaces:     []string{namespace},
				Annotation:     podCleanupTTL,
			},
		}
	}

	tests := map[string]struct {
		config       func() config.Config
		clientFunc   func(*http.Request) (*http.Response, error)
		expectedErr  error
		verifyResult func(*testing.T, string)
	}{
		"delete some pods from the list": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := generatePodList(10, namespace, nil, metav1.Time{})

					pods.Items[2].Annotations = map[string]string{podCleanupTTL: ttl10s}
					pods.Items[2].CreationTimestamp = thirtySecAgo

					pods.Items[3].Annotations = map[string]string{customTTL: ttl10s}
					pods.Items[3].CreationTimestamp = thirtySecAgo

					pods.Items[5].Annotations = map[string]string{podCleanupTTL: ttl30s}
					pods.Items[5].CreationTimestamp = tenSecAgo

					pods.Items[6].Annotations = map[string]string{customTTL: ttl30s}
					pods.Items[6].CreationTimestamp = tenSecAgo

					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, pods),
					}, nil
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						Body:       kubeclient.ObjBody(codec, &metav1.Status{}),
					}, nil
				default:
					return nil, errClientFunc
				}
			},
			verifyResult: func(t *testing.T, out string) {
				assert.Regexp(t, fmt.Sprintf(`(?m)"msg":"%s".*"name":"pod-2"`, "pod successfully deleted"), out)
			},
		},
		"reach the maximum of pods allowed": {
			config: func() config.Config {
				return config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Interval:             "1s",
					CacheCleanupInterval: "30m",
					CacheExpiration:      "1.5h",
					Limit:                5,
					MaxErrAllowed:        5,
					Kubernetes: config.KubernetesConfig{
						RequestLimit:   10,
						RequestTimeout: ttl10s,
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
					},
				}
			},
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := generatePodList(12, namespace, map[string]string{podCleanupTTL: ttl10s}, getMetaTime(-30))

					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, pods),
					}, nil
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						Body:       kubeclient.ObjBody(codec, &metav1.Status{}),
					}, nil
				default:
					return nil, errClientFunc
				}
			},
			verifyResult: func(t *testing.T, out string) {
				assert.Contains(t, out, "limit of pods to delete reached")
				assert.Regexp(
					t,
					regexp.MustCompile(
						`"level":"info","msg":"pod successfully deleted","name":"pod-[0|1|2|3|4]"`,
					),
					out,
				)
			},
		},
		"cannot delete because of invalid TTL value": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := generatePodList(10, namespace, nil, metav1.Time{})

					pods.Items[2].Annotations = map[string]string{podCleanupTTL: "not a number"}
					pods.Items[2].CreationTimestamp = thirtySecAgo

					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, pods),
					}, nil
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						Body:       kubeclient.ObjBody(codec, &metav1.Status{}),
					}, nil
				default:
					return nil, errClientFunc
				}
			},
			verifyResult: func(t *testing.T, out string) {
				assert.Contains(t, out, "not a number")
			},
		},
		"cannot delete pod because of kube error": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := generatePodList(10, namespace, nil, metav1.Time{})

					pods.Items[2].Annotations = map[string]string{podCleanupTTL: ttl10s}
					pods.Items[2].CreationTimestamp = thirtySecAgo

					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, pods),
					}, nil
				default:
					return nil, errClientFunc
				}
			},
			verifyResult: func(t *testing.T, out string) {
				assert.Contains(t, out, "error deleting pod")
			},
		},
		"detect the faulty pods": {
			config: func() config.Config {
				return config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Interval:             "1s",
					CacheCleanupInterval: "30m",
					CacheExpiration:      "1.5h",
					Limit:                10,
					MaxErrAllowed:        3,
					Kubernetes: config.KubernetesConfig{
						RequestLimit:   10,
						RequestTimeout: ttl10s,
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
					},
				}
			},
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := generatePodList(5, namespace, map[string]string{podCleanupTTL: ttl10s}, getMetaTime(-30))

					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, pods),
					}, nil
				default:
					pathParts := strings.Split(p, "/")
					switch pathParts[len(pathParts)-1] {
					case "pod-0", "pod-3":
						return nil, errClientFunc
					}

					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						Body:       kubeclient.ObjBody(codec, &metav1.Status{}),
					}, nil
				}
			},
			verifyResult: func(t *testing.T, out string) {
				assert.Regexp(t, regexp.MustCompile(`error deleting pod pod-[0|3]`), out)
				assert.Regexp(
					t,
					regexp.MustCompile(`Backing off pod deletion for .* because of the error: .* \(attempt [0|1|2]\)`),
					out,
				)
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer
			logger.SetOutput(&buf)

			fake := kubeclient.GetTestKubernetesClient(podsAPIVersion, k8sFakeClient.CreateHTTPClient(tc.clientFunc))

			podMgrOpts := newPodsManagerOpts{
				logger:     logger,
				cfg:        tc.config(),
				namespace:  namespace,
				kubeClient: fake,
			}
			pm, err := NewPodsManager(podMgrOpts)
			require.NoError(t, err)

			err = pm.logger.SetLevel(podMgrOpts.cfg.LogLevel)
			require.NoError(t, err)

			ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
			defer cancel()

			err = pm.executeDeletion(ctx)

			if tc.expectedErr != nil {
				assert.EqualError(t, err, tc.expectedErr.Error())
			}

			if tc.verifyResult != nil {
				tc.verifyResult(t, buf.String())
			}
		})
	}
}

func TestPodClean(t *testing.T) {
	version, codec := kubeclient.TestVersionAndCodec()
	maxErrAllowed := 3

	getConfig := func() config.Config {
		return config.Config{
			LogLevel:             "debug",
			LogFormat:            logging.FormatText,
			Limit:                10,
			MaxErrAllowed:        maxErrAllowed,
			Interval:             "1s",
			CacheCleanupInterval: "30m",
			CacheExpiration:      "1.5h",
			Kubernetes: config.KubernetesConfig{
				RequestLimit:   10,
				RequestTimeout: "60s",
				Namespaces:     []string{namespace},
				Annotation:     podCleanupTTL,
			},
		}
	}

	logger := logging.New()
	err := logger.SetFormat(logging.FormatJSON)
	assert.NoError(t, err)

	tests := map[string]struct {
		config       func() config.Config
		clientFunc   func(*http.Request) (*http.Response, error)
		execute      func(*testing.T, *PodsManager) error
		verifyResult func(*testing.T, string, error)
	}{
		"clean successfully pods": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					podList := generatePodList(1, namespace, map[string]string{podCleanupTTL: ttl10s}, thirtySecAgo)
					assert.NotNil(t, podList)
					assert.NotEmpty(t, podList.Items)
					return &http.Response{
						StatusCode: http.StatusOK,
						Body:       kubeclient.ObjBody(codec, podList),
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						Body:       kubeclient.ObjBody(codec, &metav1.Status{}),
					}, nil
				default:
					// Ensures no GET / DELETE is performed
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			execute: func(t *testing.T, pm *PodsManager) error {
				duration, err := time.ParseDuration(pm.cfg.Kubernetes.RequestTimeout)
				assert.NoError(t, err)
				ctx, cancel := context.WithTimeout(
					context.Background(),
					duration,
				)

				go func() {
					time.Sleep(5 * time.Second)
					cancel()
				}()

				return pm.Clean(ctx)
			},
			verifyResult: func(t *testing.T, out string, err error) {
				assert.Regexp(t, `(?m)"msg":"pod successfully deleted".*"name":"pod-0"`, out)
				assert.Contains(t, out, "context canceled")
				assert.Error(t, err)
			},
		},
		"fail to retrieve pods": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					return nil, errClientFunc
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						Body:       kubeclient.ObjBody(codec, &metav1.Status{}),
					}, nil
				default:
					// Ensures no GET / DELETE is performed
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			execute: func(t *testing.T, pm *PodsManager) error {
				duration, err := time.ParseDuration(pm.cfg.Kubernetes.RequestTimeout)
				assert.NoError(t, err)
				ctx, cancel := context.WithTimeout(
					context.Background(),
					duration,
				)
				defer cancel()

				return pm.Clean(ctx)
			},
			verifyResult: func(t *testing.T, out string, err error) {
				assert.Error(t, err)
				assert.Contains(t, out, "retrieving pod list")
			},
		},
		"fail to delete pods": {
			config: getConfig,
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					podList := generatePodList(
						maxErrAllowed, namespace,
						map[string]string{podCleanupTTL: ttl10s},
						thirtySecAgo,
					)
					assert.NotNil(t, podList)
					assert.NotEmpty(t, podList.Items)
					return &http.Response{
						StatusCode: http.StatusOK,
						Body:       kubeclient.ObjBody(codec, podList),
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				case m == http.MethodDelete:
					return nil, errClientFunc
				default:
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			execute: func(t *testing.T, pm *PodsManager) error {
				duration, err := time.ParseDuration(pm.cfg.Kubernetes.RequestTimeout)
				assert.NoError(t, err)
				ctx, cancel := context.WithTimeout(
					context.Background(),
					duration,
				)
				defer cancel()

				return pm.Clean(ctx)
			},
			verifyResult: func(t *testing.T, out string, err error) {
				assert.Error(t, err)
				assert.Contains(t, out, "error deleting pod pod-0")
				assert.Contains(t, out, "error deleting pod pod-1")
				assert.Contains(t, out, "error deleting pod pod-2")
			},
		},
		"fail to get interval": {
			config: func() config.Config {
				return config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Limit:                10,
					Interval:             "faulty",
					CacheCleanupInterval: "30m",
					CacheExpiration:      "1.5h",
					MaxErrAllowed:        maxErrAllowed,
					Kubernetes: config.KubernetesConfig{
						RequestLimit:   10,
						RequestTimeout: "60s",
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
					},
				}
			},
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					podList := generatePodList(
						maxErrAllowed, namespace,
						map[string]string{podCleanupTTL: ttl10s},
						thirtySecAgo,
					)
					assert.NotNil(t, podList)
					assert.NotEmpty(t, podList.Items)

					return &http.Response{
						StatusCode: http.StatusOK,
						Body:       kubeclient.ObjBody(codec, podList),
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						Body:       kubeclient.ObjBody(codec, &metav1.Status{}),
					}, nil
				default:
					// Ensures no GET / DELETE is performed
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			execute: func(t *testing.T, pm *PodsManager) error {
				duration, err := time.ParseDuration(pm.cfg.Kubernetes.RequestTimeout)
				assert.NoError(t, err)
				ctx, cancel := context.WithTimeout(context.Background(), duration)
				defer cancel()

				return pm.Clean(ctx)
			},
			verifyResult: func(t *testing.T, out string, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "parsing Interval time: invalid duration")
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer
			logger.SetOutput(&buf)

			fake := kubeclient.GetTestKubernetesClient(podsAPIVersion, k8sFakeClient.CreateHTTPClient(tc.clientFunc))

			podMgrOpts := newPodsManagerOpts{
				logger:     logger,
				cfg:        tc.config(),
				namespace:  namespace,
				kubeClient: fake,
			}
			pm, err := NewPodsManager(podMgrOpts)
			assert.NoError(t, err)

			err = tc.execute(t, pm)

			if tc.verifyResult != nil {
				tc.verifyResult(t, buf.String(), err)
			}
		})
	}
}

//nolint:unparam
func generatePodList(
	nbl int,
	namespace string,
	annotations map[string]string,
	creationTime metav1.Time,
) *corev1.PodList {
	p := &corev1.PodList{
		TypeMeta: metav1.TypeMeta{
			Kind:       "",
			APIVersion: "",
		},
		Items: nil,
	}

	if nbl == 0 {
		return p
	}

	p.Items = make([]corev1.Pod, 0)

	for i := 0; i < nbl; i++ {
		pod := corev1.Pod{
			TypeMeta: metav1.TypeMeta{
				Kind:       pods,
				APIVersion: podsAPIVersion,
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:         fmt.Sprintf("pod-%d", i),
				GenerateName: fmt.Sprintf("pod-%d", i),
				Namespace:    namespace,
			},
		}

		if annotations != nil {
			pod.Annotations = annotations
		}

		if !creationTime.IsZero() {
			pod.CreationTimestamp = creationTime
		}

		p.Items = append(p.Items, pod)
	}

	return p
}

func getMetaTime(ttl int64) metav1.Time {
	t := time.Now().UTC().Add(time.Duration(ttl) * time.Second)
	return metav1.NewTime(t)
}
