// +build !integration

package app

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/config"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/kubeclient"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/logging"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sFakeClient "k8s.io/client-go/rest/fake"
)

func TestClean(t *testing.T) {
	version, codec := kubeclient.TestVersionAndCodec()
	initLogger := func(t *testing.T, cfg config.Config, buf bytes.Buffer) logging.Logger {
		l := logging.New()
		err := l.SetFormat(cfg.LogFormat)
		assert.NoError(t, err)
		err = l.SetLevel(cfg.LogLevel)
		assert.NoError(t, err)
		l.SetOutput(&buf)
		return l
	}

	tests := map[string]struct {
		config      func() config.Config
		expectedErr error
	}{
		"clean successfully execute existing pods": {
			config: func() config.Config {
				return config.Config{
					LogLevel:             "debug",
					LogFormat:            logging.FormatText,
					Interval:             "1s",
					CacheCleanupInterval: "30m",
					CacheExpiration:      "1.5h",
					Limit:                10,
					MaxErrAllowed:        5,
					Kubernetes: config.KubernetesConfig{
						Namespaces:     []string{namespace},
						Annotation:     podCleanupTTL,
						RequestTimeout: "10s",
						RequestLimit:   10,
					},
				}
			},
			expectedErr: fmt.Errorf("context deadline exceeded"),
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer

			cfg := tc.config()
			logger := initLogger(t, cfg, buf)
			duration, err := cfg.GetRequestTimeout()
			assert.NoError(t, err)

			ctx, cancel := context.WithTimeout(context.Background(), duration)
			defer cancel()

			clientFunc := func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/Namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := &corev1.PodList{
						TypeMeta: metav1.TypeMeta{
							Kind:       "",
							APIVersion: "",
						},
						Items: nil,
					}
					return &http.Response{
						Header:     map[string][]string{"Content-Type": {"application/json"}},
						StatusCode: http.StatusOK,
						Status:     http.StatusText(http.StatusOK),
						Body:       kubeclient.ObjBody(codec, pods),
					}, nil
				default:
					return nil, fmt.Errorf("unexpected request")
				}
			}

			fake := kubeclient.GetTestKubernetesClient(podsAPIVersion, k8sFakeClient.CreateHTTPClient(clientFunc))

			cleaner := &KubernetesCleaner{
				logger:     logger,
				cfg:        cfg,
				kubeClient: fake,
			}

			err = cleaner.Clean(ctx)
			if tc.expectedErr != nil {
				assert.EqualError(t, err, tc.expectedErr.Error())
			}
		})
	}
}
